<?php

/**
 * CHILD THEME FUNCTIONS
 *
 * @link https://developer.wordpress.org/themes/advanced-topics/child-themes/#what-is-a-child-theme
 */

/**
 * Include all child theme functions
 * 
 * @since   0.1
 * @access  private
 */

require get_stylesheet_directory() . '/functions/awsm-child.php'; 

/**
 * Register all of the hooks related to the admin area functionality
 * of the plugin.
 *
 * @since   0.1
 * @access  private
 */

// Admin hooks go here ..

 /**
 * Register all of the hooks related to the public-facing functionality
 * of the theme.
 *
 * @since   0.1
 * @access  private
 */

add_action( 'wp_enqueue_scripts', 'awsm_child_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'awsm_child_enqueue_scripts' );

<?php 

/**
 * Include all core functions
 * 
 * @since   0.1
 * @access  private
 */

require get_stylesheet_directory() . '/functions/core/styles.php';

/**
 * Include all helper functions
 * 
 * @since   0.1
 * @access  private
 */

require get_stylesheet_directory() . '/functions/helpers/scripts.php';

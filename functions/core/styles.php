<?php

/**
 * Enqueue all the styles thats needed to get the child theme working. 
 * 
 * @since   0.1
 * @link https://developer.wordpress.org/themes/basics/including-css-javascript/
 */

function awsm_child_enqueue_styles() {
	/** 
	 * This is 'awsm-style' for the Awesomatic theme. 
	 */
	$parent_style = 'awsm-style'; 
	/**
	 * Enqueue the parent style 
	 */
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	/**
	 * Enqueue the child style after the parent style and give it a version 
	 */
	wp_enqueue_style( 'awsm-child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version') );
}

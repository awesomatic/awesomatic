<?php

/**
 * ENQUEUE SCRIPTS
 * 
 * wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer);
 * 
 * $handle is the name for the script. 
 * $src defines where the script is located.
 * $deps is an array that can handle any script that your new script depends on, such as jQuery.
 * $ver lets you list a version number.
 * $in_footer is a boolean parameter (true/false (=header)) that allows you to place your scripts in the footer
 * 
 * @since 0.1
 * @link https://developer.wordpress.org/themes/basics/including-css-javascript/
 */

function awsm_child_enqueue_scripts() {
	/**
	 * jQuery - Elastic Header
	 */
	wp_enqueue_script( 'elastic-header', get_stylesheet_directory_uri() . '/scripts/elastic-header.js', array(), '1.0.0', true );
}
